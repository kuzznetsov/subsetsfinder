package ua.kep.SubsetsFinder;

import java.util.ArrayList;

class Subset {

    ArrayList<String> subsetDef(String[] input) {

    ArrayList<String> resultList = new ArrayList<>();
    String stringToResultList = "";
    int inputLength = input.length;
        
//         It creates an instance in which add to sting {} and remove last commas
        OutputStringFormatter formatter = new OutputStringFormatter();

        for (int i = 0; i < (1 << inputLength); i++) {
            for (int j = 0; j < inputLength; j++) {
                if ((i & (1 << j)) != 0) {

                    stringToResultList += input[j] + ",";
                }
            }

            resultList.add(formatter.strFormatter(stringToResultList));

            stringToResultList = "";

        }
        return resultList;
    }
}
