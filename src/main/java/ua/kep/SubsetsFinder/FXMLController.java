package ua.kep.SubsetsFinder;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLController implements Initializable {

    private String inputSequence;

    @FXML
    private Label label;

    @FXML
    private void inputAction(ActionEvent in) {

        inputSequence = ((TextField) in.getSource()).getText();

        Response responce = new Response();

        label.setText(responce.toResponse(inputSequence));
    }

    @FXML
    private TextField tf;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        tf.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("(?!.*[\\.,]{2,})[\\d,\\.]*")) {
                ((StringProperty) observable).setValue(oldValue);
            }
        });

    }
}
