package ua.kep.SubsetsFinder;

import java.util.ArrayList;

class Response {
    
    String toResponse(String inputString) {
        
    String toLableString;
    ArrayList<String> listFromSubsetDef;
    StringBuilder out;
    
        if (inputString.matches("\\..*") || inputString.matches("\\,.*")
                || inputString.matches("\\d+\\.\\d+\\..*")
                || inputString.matches(".*\\.")||inputString.matches(".*\\.\\d+\\..*")) {
            toLableString = ("Empty or incorrect input! Try again.");
        } else {

//            It creates an instance in which string is converted into array of strings
            Converter converter = new Converter();

//            It creates an instance in which takes all subsets of set
            Subset subset = new Subset();

            listFromSubsetDef = subset.subsetDef(converter.convertStringToArray(inputString));

//             It makes response string from list of subsets
            out = new StringBuilder();
            out.append("Subsets are:\n");
            for (String str : listFromSubsetDef) {
                out.append(str);
                out.append("\n");
            }
            toLableString = out.toString();
        }
        return toLableString;
    }
}
