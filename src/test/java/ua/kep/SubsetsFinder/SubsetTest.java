package ua.kep.SubsetsFinder;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class SubsetTest {
    
    public SubsetTest() {
    }
    
    @Test
    public void testSubsetDef() {
        System.out.println("subsetDef");
        String[] input = {"1","0","7.3"};
        Subset instance = new Subset();
        ArrayList expResult = new ArrayList();
        expResult.add("{}");
        expResult.add("{1}");
        expResult.add("{0}");
        expResult.add("{1,0}");
        expResult.add("{7.3}");
        expResult.add("{1,7.3}");
        expResult.add("{0,7.3}");
        expResult.add("{1,0,7.3}");
        ArrayList result = instance.subsetDef(input);
        assertEquals(expResult, result);
        
    }
    
}
