package ua.kep.SubsetsFinder;

import org.junit.Test;
import static org.junit.Assert.*;

public class ResponceTest {
    
    public ResponceTest() {
    }
        
    @Test
    public void testToResponce() {
        System.out.println("toResponce");
        String inputString = "1,0,7.3";
        Response instance = new Response();
        String expResult = 
       "Subsets are:\n" +
        "{}\n" +
        "{1}\n" +
        "{0}\n" +
        "{1,0}\n" +
        "{7.3}\n" +
        "{1,7.3}\n" +
        "{0,7.3}\n" +
        "{1,0,7.3}\n" +
        "";
        String result = instance.toResponse(inputString);
        assertEquals(expResult, result);
        
    }
    
}
