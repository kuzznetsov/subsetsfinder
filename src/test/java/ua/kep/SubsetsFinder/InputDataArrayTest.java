
package ua.kep.SubsetsFinder;

import org.junit.Test;
import static org.junit.Assert.*;

public class InputDataArrayTest {
    
    public InputDataArrayTest() {
    }

    @Test
    public void testInputToArray() {
        System.out.println("inputToArray");
        String input = "1,0,7.3";
        Converter instance = new Converter();
        String[] expResult = {"1","0","7.3"};
        String[] result = instance.convertStringToArray(input);
        assertArrayEquals(expResult, result);
        }
    
}
