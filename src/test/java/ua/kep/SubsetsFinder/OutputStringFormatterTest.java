package ua.kep.SubsetsFinder;

import org.junit.Test;
import static org.junit.Assert.*;

public class OutputStringFormatterTest {
    
    public OutputStringFormatterTest() {
    }

    @Test
    public void testStrFormatter() {
        System.out.println("strFormatter");
        String str = "1,0,7.3,";
        OutputStringFormatter instance = new OutputStringFormatter();
        String expResult = "{1,0,7.3}";
        String result = instance.strFormatter(str);
        assertEquals(expResult, result);
        
    }
    
}
